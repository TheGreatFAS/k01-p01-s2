#pragma once

#ifndef MAINTITLE_H
#define MAINTITLE_H

#include <QDebug>
#include <iostream>
#include <QtWidgets>

using namespace std;

class Maintitle : public QWidget
{
	Q_OBJECT
public:
	Maintitle(QWidget* parent);
	~Maintitle();
	QPushButton* getBoutonDemarrer();
	QPalette getBackground();
private slots:
	void exit();
private:
	QPushButton* button1;
	QPushButton* button2;
	QGridLayout* layout;
	QAction* quit = nullptr;
	QMenu* File = nullptr;
	QMenuBar* menubar = nullptr;
	QPalette paletteBackGround;

};

#endif // MAINTITLE_H