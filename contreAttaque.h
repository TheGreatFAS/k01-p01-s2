#pragma once
using namespace std;
#include <iostream>
#include <string.h>
#include <string>

class ContreAttaque
{
private:
	int degatEvites;
	string nom;
	/*Type type;
	Image image;
	Animation animation;*/
public:
	ContreAttaque(string, int);
	~ContreAttaque();
	string getNom() {return nom; }
	int getDegatsEvites() { return degatEvites;}
	/*Type getType(){}return type;
	Image getImage() {return image;}
	Animation getAnimation(){returnAnimation}*/
	void setDegatsEvites(int n) { degatEvites = n; }
	void afficherConsole();
};

