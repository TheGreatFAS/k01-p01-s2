#pragma once
#ifndef KEYPRESS_H
#define KEYPRESS_H

#include <QWidget> 
#include <QKeyEvent>
#include <QDebug>
#include <QGraphicsItem>

using namespace std;

class KeyPress : public QGraphicsItem
{
	Q_OBJECT

public:
	KeyPress(QWidget *parent = 0);

protected:
	void keyPressEvent(QKeyEvent *);
};
#endif