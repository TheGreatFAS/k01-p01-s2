#ifndef ATTAQUE_H
#define ATTAQUE_H

#include <iostream>
#include "type.h"

using namespace std;

class Attaque
{
    public:
        Attaque();
        Attaque(Type* type1, Type* typeFaiblesse, int degats1, string nom);
        Type* getFaiblesse();
        int getDegats();
        Type* getType();
        string getNomType();
        string getNom();
        Type::lesTypes getEnumType();
        
    private:
        Type* type;
        Type* faiblesse;
        int degats;
        string nom;
};

#endif