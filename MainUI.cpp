#include "MainUI.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
	this->activateWindow();
	this->setFocus();
	mainWidget = new QWidget(this);

	QFont fontBouton("Times", 30);

	afficherMenuPrincipal();
}

MainWindow::~MainWindow()
{

}

void MainWindow::afficherMenuSelection()
{
	Menu = new MenuSelectionJoueur(this);
	boutonCancelSelection = new QPushButton("Retourner au Menu Principal", Menu->getMainWidget());
	boutonCancelSelection->setFixedSize(500, 50);
	boutonCancelSelection->move(710, 900);
	connect(boutonCancelSelection, SIGNAL(clicked()), this, SLOT(afficherMenuPrincipalSelection()));
	connect(Menu->getBoutonJouer(), SIGNAL(clicked()), this, SLOT(afficherCombat()));
	mainWidget = takeCentralWidget();
	this->setPalette(Menu->getBackground());
	setCentralWidget(Menu->getMainWidget());
	boutonCancelSelection->setVisible(true);
}

QPushButton* MainWindow::getMainButton()
{
	return bouton;
}

void MainWindow::afficherMenuPrincipal()
{
	menuPrincipal = new Maintitle(this);
	this->setPalette(menuPrincipal->getBackground());
	connect(menuPrincipal->getBoutonDemarrer(), SIGNAL(clicked()), this, SLOT(afficherMenuSelection()));
	setCentralWidget(menuPrincipal);
}

void MainWindow::afficherMenuPrincipalSelection()
{
	this->setFocus();
	delete Menu;
	takeCentralWidget();
	boutonCancelSelection->setVisible(false);
	QBrush brush(Qt::white);
	paletteBackground.setBrush(QPalette::Background, brush);
	this->setPalette(menuPrincipal->getBackground());
	setCentralWidget(menuPrincipal);
}

void MainWindow::afficherFinal()
{
	//takeCentralWidget();
	//delete combat;
	//Win = new win();
	//this->setPalette(Win->getBackground());
	//setCentralWidget(Win->getMainWidget());
	
	
	Win = new win(combat->getWinner(), this);
	boutonCancelFinal = new QPushButton("Retourner au Menu Principal", Win->getMainWidget());
	boutonCancelFinal->setFixedSize(500, 50);
	boutonCancelFinal->move(710, 900);
	connect(boutonCancelFinal, SIGNAL(clicked()), this, SLOT(afficherMenuPrincipalFinal()));
	takeCentralWidget();
	this->setPalette(Win->getBackground());
	setCentralWidget(Win->getMainWidget());
	boutonCancelSelection->setVisible(true);
	
}

void MainWindow::afficherMenuPrincipalFinal()
{
	this->setFocus();
	delete Win;
	takeCentralWidget();
	boutonCancelFinal->setVisible(false);
	afficherMenuPrincipal();
}

QWidget* MainWindow::getMainWidget()
{
	return mainWidget;
}

void MainWindow::afficherCombat()
{
	combat = new Combat(Menu, Menu->getPersonnages());
	continuerCombat = new QPushButton("Voir le gagnant!", combat);
	continuerCombat->setVisible(true);
	continuerCombat->move(900, 500);
	continuerCombat->setFixedSize(200, 40);
	continuerCombat->setFont(fontBouton);
	connect(continuerCombat, SIGNAL(clicked()), this, SLOT(afficherFinal()));
	combat->setBoutonContinuer(continuerCombat);
	Menu->setMainWidget(takeCentralWidget());
	this->setPalette(combat->getBackground());
	setCentralWidget(combat);
}