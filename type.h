#pragma once

#include <iostream>

using namespace std;

class Type
{
    public:
        Type();
        enum lesTypes
        {
            Defense = 1,
            Professeur,
            Psychique,
            Physique,
            Depression
        };
        Type(string, Type*, double, double, Type::lesTypes);
        std::string getNom();
        Type* getFaiblesse();
        double getFaiblessePuissance();
        double getForce();
        lesTypes getEnumType();

    private:
        std::string nom;
        Type* faiblesse;
        double faiblessePuissance;
        double force;
        lesTypes enumType;
};