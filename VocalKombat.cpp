/*
*   Ce programme exécute le code 
*   pour le jeu VocalKombat de l'équipe
*   K1-P01 (Promo 65)
*/

#include "Personnage.h"
#include <iostream>
#include "Attaque.h"
#include "type.h"
#include "replique.h"
#include "vecteur.h"
#include "menu.h"
#include <stdlib.h>
#include <time.h>

int main()
{

    //DÉBUT DES TESTS
    srand(time(NULL));
    int joueurActuel = (rand()%2);

    cout << "Test menu" << endl;

    Vecteur<Personnage*> players;

    Menu menu;

    players += menu.characterSelect();
    players += menu.characterSelect();

    players[0]->setAdversaire(players[1]);
    players[1]->setAdversaire(players[0]);

    cout << CONSOLECOLOR_MAGENTA << "Joueur " << joueurActuel + 1 << " commence." << CONSOLECOLOR_WHITE << endl;

    while(players[1]->getVie() > 0 && players[0]->getVie() > 0)
    {
        cout << CONSOLECOLOR_MAGENTA << players[joueurActuel]->getNom() << " choisi son attaque parmis les attaques suivantes:" 
        << CONSOLECOLOR_WHITE<< endl;
        int choixAttaque = 1;
        menu.afficherAttaques(players[joueurActuel]);
        cout << CONSOLECOLOR_MAGENTA <<"Votre choix (De 1 à " << players[joueurActuel]->getAttaques()->getTaille() << "):" << CONSOLECOLOR_WHITE;
        cin >> choixAttaque;
        players[joueurActuel]->attaquer(choixAttaque - 1);
        if(joueurActuel == 0)
        {
            joueurActuel = 1;
        } else
        {
            joueurActuel = 0;
        }
    }

    for(size_t i = 0; i < players.getTaille(); i++)
    {
        if(players[i]->getVie() <= 0)
        {
            cout << CONSOLECOLOR_MAGENTA << players[i]->getNom() << " n'a plus de vie!" << endl << "Fin de la partie!" << CONSOLECOLOR_RESET <<endl;
        }
    }

    return 0;
}
