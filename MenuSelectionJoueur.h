#pragma once

#ifndef SELECTION_H
#define SELECTION_H

#include "Personnages/Personnage.h"
#include "Personnages/Claudette.h"
#include "Personnages/JP.h"
#include "Personnages/Etudiant.h"
#include "Personnages/GRO.h"
#include "vecteur.h"
#include <QDebug>
#include <iostream>
#include <QtWidgets>

using namespace std;

class MenuSelectionJoueur
	: public QWidget
{
	Q_OBJECT
public:
	MenuSelectionJoueur(QWidget *parent);
	~MenuSelectionJoueur();
	QWidget* getMainWidget();
	QPalette getBackground();
	void setMainWidget(QWidget*);
	QPushButton* getBoutonJouer();
	Vecteur<Personnage*> getPersonnages();
	
private:
	Vecteur<Personnage*> personnages;
	JP* jp;
	Claudette* claudette;
	Etudiant* etudiant;
	GRO* gro;
	QHBoxLayout *layoutHorizontal;
	QVBoxLayout *layoutVerticalGauche;
	QVBoxLayout *layoutVerticalDroite;
	QWidget *m_mainWidget;
	QWidget *m_combattantsWidget;
	QVBoxLayout *layoutPrincipal;
	QLabel *titre;
	QFont f;
	QFont boutonPlay;
	QPixmap backGroundImage;
	QGraphicsScene* scene;
	QGraphicsView* view;
	QPalette paletteBackGround;
	QLabel* nom;
	QLabel* actualPlayer;
	QPushButton* jpBtn;
	QPushButton* claudetteBtn;
	QPushButton* etudiantBtn;
	QPushButton* groBtn;
	QPushButton* selectionnerBtn;
	QPushButton* jouerBtn;
	int player = 1;

private slots:
	void selectionJP();
	void selectionClaudette();
	void selectionEtudiant();
	void selectionGRO();
	void jouer();
};



#endif