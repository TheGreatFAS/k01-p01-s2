#include "MenuSelectionJoueur.h"

MenuSelectionJoueur::MenuSelectionJoueur(QWidget* parent)
	:QWidget(parent)
{
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
	//Initialisations
	this->setFocus();
	QFont f("Helvetica", 40, QFont::ExtraBold);
	QFont boutonJouer("Helvetica", 20, QFont::Bold);
	m_mainWidget = new QWidget();
	m_combattantsWidget = new QWidget(m_mainWidget);
	layoutPrincipal = new QVBoxLayout(m_mainWidget);
	QPalette palette;
	titre = new QLabel("Choisis ton combattant!", m_mainWidget);
	palette.setColor(QPalette::WindowText, Qt::red);
	layoutHorizontal = new QHBoxLayout(m_combattantsWidget);
	layoutVerticalGauche = new QVBoxLayout(m_combattantsWidget);
	layoutVerticalDroite = new QVBoxLayout(m_combattantsWidget);
	view = new QGraphicsView(this);
	QPixmap backGroundImage("./Images/Background_Selection.png");
	backGroundImage.scaled(this->size(), Qt::IgnoreAspectRatio);
	paletteBackGround.setBrush(QPalette::Background, backGroundImage);
	claudette = new Claudette();
	etudiant = new Etudiant();
	gro = new GRO();
	jp = new JP();
	QString texteBouton("Selectionner");
	texteBouton = texteBouton.toUtf8();
	actualPlayer = new QLabel("Joueur " + QString::number(player), m_mainWidget);
	selectionnerBtn = new QPushButton(texteBouton, m_mainWidget);
	jouerBtn = new QPushButton("Jouer!", m_mainWidget);

	personnages += nullptr;
	personnages += nullptr;

	selectionnerBtn->setFixedSize(200, 100);
	selectionnerBtn->move(860, 490);
	selectionnerBtn->setVisible(false);
	selectionnerBtn->setFont(boutonJouer);

	jouerBtn->setFixedSize(200, 100);
	jouerBtn->move(860, 490);
	jouerBtn->setVisible(false);
	jouerBtn->setFont(boutonJouer);


	titre->setPalette(palette);
	titre->setFont(f);
	actualPlayer->setPalette(palette);
	actualPlayer->setFont(f);

	layoutPrincipal->addWidget(titre, 0, Qt::AlignCenter);
	layoutPrincipal->addWidget(actualPlayer, 0, Qt::AlignCenter);
	m_combattantsWidget->setLayout(layoutHorizontal);
	layoutPrincipal->addWidget(m_combattantsWidget);
	
	m_mainWidget->setLayout(layoutPrincipal);
	QPalette paletteNom;
	paletteNom.setColor(QPalette::WindowText, Qt::blue);
	QFont names("Times", 20, QFont::Bold);

	//Ajout du bouton de JP
	jpBtn = new QPushButton(m_combattantsWidget);
	nom = new QLabel(QString::fromStdString(jp->getNom()), m_combattantsWidget);
	QPixmap imgJP(QString::fromStdString(jp->pathToSpriteTitle));
	QPalette palJP;
	palJP.setBrush(jpBtn->backgroundRole(), QBrush(imgJP));
	jpBtn->setFlat(true);
	jpBtn->setAutoFillBackground(true);
	jpBtn->setPalette(palJP);
	jpBtn->setFixedHeight(300);
	jpBtn->setFixedWidth(300);
	nom->setPalette(paletteNom);
	nom->setFont(names);
	layoutVerticalGauche->addWidget(jpBtn, 0, Qt::AlignCenter);
	layoutVerticalGauche->addWidget(nom, 0, Qt::AlignCenter);
	layoutVerticalGauche->addSpacing(50);

	//Ajout du bouton de Claudette
	claudetteBtn = new QPushButton(m_combattantsWidget);
	nom = new QLabel(QString::fromStdString(claudette->getNom()), m_combattantsWidget);
	QPixmap imgClaudette(QString::fromStdString(claudette->pathToSpriteTitle));
	QPalette palClaudette;
	palClaudette.setBrush(claudetteBtn->backgroundRole(), QBrush(imgClaudette));
	claudetteBtn->setFlat(true);
	claudetteBtn->setAutoFillBackground(true);
	claudetteBtn->setPalette(palClaudette);
	claudetteBtn->setFixedHeight(300);
	claudetteBtn->setFixedWidth(300);
	nom->setPalette(paletteNom);
	nom->setFont(names);
	layoutVerticalGauche->addWidget(claudetteBtn, 0, Qt::AlignCenter);
	layoutVerticalGauche->addWidget(nom, 0, Qt::AlignCenter);
	layoutVerticalGauche->addSpacing(50);

	//Ajout du bouton de Etudiant
	etudiantBtn = new QPushButton(m_combattantsWidget);
	nom = new QLabel(QString::fromStdString(etudiant->getNom()), m_combattantsWidget);
	QPixmap imgEtudiant(QString::fromStdString(etudiant->pathToSpriteTitle));
	QPalette palEtudiant;
	palEtudiant.setBrush(etudiantBtn->backgroundRole(), QBrush(imgEtudiant));
	etudiantBtn->setFlat(true);
	etudiantBtn->setAutoFillBackground(true);
	etudiantBtn->setPalette(palEtudiant);
	etudiantBtn->setFixedHeight(300);
	etudiantBtn->setFixedWidth(300);
	nom->setPalette(paletteNom);
	nom->setFont(names);
	layoutVerticalDroite->addWidget(etudiantBtn, 0, Qt::AlignCenter);
	layoutVerticalDroite->addWidget(nom, 0, Qt::AlignCenter);
	layoutVerticalDroite->addSpacing(50);

	//Ajout du bouton de GRO
	groBtn = new QPushButton(m_combattantsWidget);
	nom = new QLabel(QString::fromStdString(gro->getNom()), m_combattantsWidget);
	QPixmap imgGro(QString::fromStdString(gro->pathToSpriteTitle));
	QPalette palGro;
	palGro.setBrush(groBtn->backgroundRole(), QBrush(imgGro));
	groBtn->setFlat(true);
	groBtn->setAutoFillBackground(true);
	groBtn->setPalette(palGro);
	groBtn->setFixedHeight(300);
	groBtn->setFixedWidth(300);
	nom->setPalette(paletteNom);
	nom->setFont(names);
	layoutVerticalDroite->addWidget(groBtn, 0, Qt::AlignCenter);
	layoutVerticalDroite->addWidget(nom, 0, Qt::AlignCenter);
	layoutVerticalDroite->addSpacing(50);

	//Ajout des layouts au layout principal
	layoutHorizontal->addLayout(layoutVerticalGauche);
	layoutHorizontal->addLayout(layoutVerticalDroite);
	
	//Connexion des boutons
	connect(jpBtn, SIGNAL(clicked()), this, SLOT(selectionJP()));
	connect(claudetteBtn, SIGNAL(clicked()), this, SLOT(selectionClaudette()));
	connect(etudiantBtn, SIGNAL(clicked()), this, SLOT(selectionEtudiant()));
	connect(groBtn, SIGNAL(clicked()), this, SLOT(selectionGRO()));
	connect(jouerBtn, SIGNAL(clicked()), this, SLOT(jouer()));
	connect(selectionnerBtn, SIGNAL(clicked()), this, SLOT(jouer()));
}

MenuSelectionJoueur::~MenuSelectionJoueur()
{
	
}

QWidget* MenuSelectionJoueur::getMainWidget()
{
	return m_mainWidget;
}

QPalette MenuSelectionJoueur::getBackground()
{
	return paletteBackGround;
}

void MenuSelectionJoueur::selectionJP()
{
	if (player == 1)
	{
		if (personnages[0] != nullptr)
		{
			delete personnages[0];
		}
		selectionnerBtn->setVisible(true);
		personnages[0] = new JP();
	}
	else
	{
		if (personnages[1] != nullptr)
		{
			delete personnages[1];
		}
		personnages[1] = new JP();
		jouerBtn->setEnabled(true);
	}
}

void MenuSelectionJoueur::selectionClaudette()
{
	if (player == 1)
	{
		if (personnages[0] != nullptr)
		{
			delete personnages[0];
		}
		selectionnerBtn->setVisible(true);
		personnages[0] = new Claudette();
	}
	else 
	{
		if (personnages[1] != nullptr)
		{
			delete personnages[1];
		}
		personnages[1] = new Claudette();
		jouerBtn->setEnabled(true);
	}
}

void MenuSelectionJoueur::selectionEtudiant()
{
	if (player == 1)
	{
		if (personnages[0] != nullptr)
		{
			delete personnages[0];
		}
		selectionnerBtn->setVisible(true);
		personnages[0] = new Etudiant();
	}
	else
	{
		if (personnages[1] != nullptr)
		{
			delete personnages[1];
		}
		personnages[1] = new Etudiant();
		jouerBtn->setEnabled(true);
	}
}

void MenuSelectionJoueur::selectionGRO()
{
	if (player == 1)
	{
		if (personnages[0] != nullptr)
		{
			delete personnages[0];
		}
		selectionnerBtn->setVisible(true);
		personnages[0] = new GRO();
	}
	else
	{
		if (personnages[1] != nullptr)
		{
			delete personnages[1];
		}
		personnages[1] = new Etudiant();
		jouerBtn->setEnabled(true);
	}
}

void MenuSelectionJoueur::jouer()
{
	if (player == 1)
	{
		player = 2;
		actualPlayer->setText("Joueur " + QString::number(player));
		jouerBtn->setVisible(true);
		jouerBtn->setEnabled(false);
	}
	else
	{
		personnages[0]->setAdversaire(personnages[1]);
		personnages[1]->setAdversaire(personnages[0]);
	}
}

void MenuSelectionJoueur::setMainWidget(QWidget* nouvWidget)
{
	m_mainWidget = nouvWidget;
}

QPushButton* MenuSelectionJoueur::getBoutonJouer()
{
	return jouerBtn;
}

Vecteur<Personnage*> MenuSelectionJoueur::getPersonnages()
{
	return personnages;
}