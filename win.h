#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QDebug>
#include <iostream>

using namespace std;
class win : public QWidget
{
	Q_OBJECT
public:
	win(int winnerr, QWidget *parent = nullptr);
	~win();
	QWidget* getMainWidget();
	QPalette getBackground();

private:
	QLabel* winner;
	QPixmap winimg;
	QPixmap background;
	QVBoxLayout *layoutprincipal;
	QVBoxLayout *layoutbouton;
	QWidget *mainwidget;
	QWidget *boutwidget;
	QPalette paletteBackGround;
	QWidget *espace;
	QWidget *espace2;
	QWidget *espace3;

};
#endif // MAINWINDOW_H
