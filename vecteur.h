#ifndef VECTEUR_H
#define VECTEUR_H
#include <string.h>
#include <ostream>
#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Vecteur
{
private:
	T *tab;
	int cap;
	int taille;
	int positionLiveItem;
public:
	Vecteur();
	Vecteur(const Vecteur& copysource);
	~Vecteur();
	int getCap() const;
	int getTaille() const;
	int getPosition() const;
	void operator++();
	void operator--();
	T& operator[](int index);
	bool operator+=(T ptr);
	bool operator-=(T ptr);
	friend ostream& operator<<(ostream& s, Vecteur<T>& vec)
	{
		for (int i = 0; i < vec.taille; i++)
		{
			s << vec[i] << endl;
		}

		return s;
	}
	void vider();
	bool estVide();
};
//constructeur et destructeur//
template<typename T>
Vecteur<T>::Vecteur()
{
	cap = 10;
	taille = 0;
	tab = new T[cap];
	positionLiveItem = 0;
}

template<typename T>
Vecteur<T>::Vecteur(const Vecteur& copysource)
{
	cap = copysource.cap;
	taille = copysource.taille;
	tab = new T[cap];
	for (int i = 0; i < taille; i++)
	{
		tab[i] = copysource.tab[i];
	}
	positionLiveItem = copysource.positionLiveItem;
}
template<typename T>
Vecteur<T>::~Vecteur()
{
	if (taille != 0)
	{
		delete[] tab;
	}
}
//fin des constructeurs et destructeurs//
////////////////////////////////////////
//accesseur de la classe//
template<typename T>
int Vecteur<T>::getCap() const
{
	return cap;
}

template<typename T>
int Vecteur<T>::getTaille() const
{
	return taille;
}

template<typename T>
int Vecteur<T>::getPosition() const
{
	return positionLiveItem;
}
//fin des accesseurs de la classe//
//////////////////////////////////////////
//methode de deplacement dans le vecteur//
template<typename T>
void Vecteur<T>::operator++()
{
	if (positionLiveItem < taille - 1)
	{
		positionLiveItem++;
	}
}

template<typename T>
void Vecteur<T>::operator--()
{
	if (positionLiveItem > 0)
	{
		positionLiveItem--;
	}
}
//fin des methodes de deplacement dans le vecteur//
//////////////////////////////////////////////////
//methodes d'ajout d'objet en mode pile et file//
template<typename T>
bool Vecteur<T>::operator-=(T ptr)
{
	if (taille >= cap - 1)
	{
		T *temp = new T[cap * 2];
		for (int i = taille; i >= 1; i--)
		{
			temp[i] = tab[i - 1];
		}
		delete[] tab;
		cap = cap * 2;
		tab = temp;
	}
	else
	{
		for (int i = taille; i >= 1; i--)
		{
			tab[i] = tab[i - 1];
		}
	}
	tab[0] = ptr;
	taille++;
	return true;
}
template<typename T>
bool Vecteur<T>::operator+=(T ptr)
{
	if (taille >= cap)
	{
		T *temp = new T[cap * 2];
		for (int i = 0; i < taille; i++)
		{
			temp[i] = tab[i];
		}
		for (int i = taille; cap * 2; i++)
		{
			temp[i] = nullptr;
		}
		delete[] tab;
		cap = cap * 2;
		tab = temp;
	}
	tab[taille] = ptr;
	taille++;
	return true;
}
//fin des methodes d'ajout d'objet en mode pile et file//
/////////////////////////////////////////////////////////
//methode d'accession a un element//
template<typename T>
T& Vecteur<T>::operator[](int index)
{
	//T b = NULL;
	if (!this->estVide())
	{
		int a = index;
		if (a >= taille)
		{
			return tab[0] ;
		}
		return tab[a];
	}
	else { return tab[0]; }
}
//fin de la methode d'accession a un element//
//////////////////////////////////////////////
//methode d'impression du vecteur//
// template<typename T>

// ostream & operator<<(ostream & s, const Vecteur<T> & vec)
// {
// 	if (vec.getTaille()!= 0)
// 	{
// 		for (int i = 0; i < vec.getTaille(); i++)
// 		{
// 			s << vec.tab[i] << " ";
// 		}
// 	}
// 	s << endl;
// 	return s;
// }
//fin de la methode d'impression du vecteur//
/////////////////////////////////////////////
//methodes de vidage du vecteur//
template<typename T>
void Vecteur<T>::vider()
{
	if (taille != 0)
	{
		T* temp = new T[10];
		delete [] tab;
		tab = temp;
		taille = 0;
	}
}

template<typename T>
bool Vecteur<T>::estVide()
{
	if (taille == 0) { return true; }
	else { return false; }
}
//fin des methodes de vidages du vecteurs//
#endif
