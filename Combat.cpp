#include "Combat.h"

Combat::Combat(QWidget* parent1, Vecteur<Personnage*> personnages1) : QWidget(parent1)
{
	QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
	personnages += personnages1[0];
	personnages += personnages1[1];
	QPixmap backgroundImage("./Images/Background_Combat.png");
	backgroundImage.scaled(this->size(), Qt::IgnoreAspectRatio);
	paletteBackground.setBrush(QPalette::Background, backgroundImage);
	HPPlayer1 = new QProgressBar(this);
	HPPlayer2 = new QProgressBar(this);
	/*QPixmap pixmapP1(QString::fromStdString(personnages[0]->pathToSpriteP1));
	QPixmap pixmapP2(QString::fromStdString(personnages[1]->pathToSpriteP2));
	QPalette palP1;
	QPalette palP2;
	persoP1 = new QWidget(this);
	persoP2 = new QWidget(this);
	layoutBonhommes = new QHBoxLayout(this);*/
	texteAction = new QLabel("DEBUT DU COMBAT!", this);
	QFont fTitre("Helvetica", 40, QFont::ExtraBold);
	QPalette paletteTitre;
	paletteTitre.setColor(QPalette::WindowText, Qt::black);
	QFont fAction("Times", 15, QFont::Bold);
	QPalette paletteAction;
	paletteAction.setColor(QPalette::WindowText, Qt::white);
	jp = new JP();
	claudette = new Claudette();
	etudiant = new Etudiant();
	gro = new GRO();



	//D�placement des barres de progr�s
	HPPlayer1->move(165, 235);
	HPPlayer2->move(1200, 245);

	//D�finition du style des barres
	HPPlayer1->setStyleSheet(" QProgressBar { border-radius: 10px; background-color: black;} QProgressBar::chunk {background-color: #3add36; border-radius: 5px; margin: 3px;}");
	HPPlayer2->setStyleSheet(" QProgressBar { border-radius: 10px; background-color: black;} QProgressBar::chunk {background-color: #3add36; border-radius: 5px; margin: 3px;}");

	this->setFocusPolicy(Qt::StrongFocus);
	this->setFocus();
	
	//D�finition du minimum et du maximum des barres
	HPPlayer1->setMinimum(0);
	HPPlayer2->setMinimum(0);

	HPPlayer1->setMaximum(personnages[0]->getVie());
	HPPlayer2->setMaximum(personnages[1]->getVie());

	if (personnages[0]->getPtsDefense() > 0)
	{
		DefPlayer1 = new QProgressBar(this);
		DefPlayer1->move(210, 195);
		DefPlayer1->setMinimum(0);
		DefPlayer1->setMaximum(personnages[0]->getPtsDefense());
		DefPlayer1->setTextVisible(false);
		DefPlayer1->setFixedSize(545, 30);
		DefPlayer1->setStyleSheet(" QProgressBar { border-radius: 10px; background-color: black;} QProgressBar::chunk {background-color: #0fe4f0; border-radius: 5px; margin: 3px;}");
	}

	if (personnages[1]->getPtsDefense() > 0)
	{
		DefPlayer2 = new QProgressBar(this);
		DefPlayer2->move(1245, 205);
		DefPlayer2->setMinimum(0);
		DefPlayer2->setMaximum(personnages[1]->getPtsDefense());
		DefPlayer2->setTextVisible(false);
		DefPlayer2->setFixedSize(545, 30);
		DefPlayer2->setStyleSheet(" QProgressBar { border-radius: 10px; background-color: black;} QProgressBar::chunk {background-color: #0fe4f0; border-radius: 5px; margin: 3px;}");
	}

	//Fait disparaitre le texte des barres de progr�s
	HPPlayer1->setTextVisible(false);
	HPPlayer2->setTextVisible(false);

	//D�finition de la taille des barres
	HPPlayer1->setFixedSize(590, 30);
	HPPlayer2->setFixedSize(590, 30);

	//Met � jour les barres de progr�s selon la vie et les pts de d�fense des personnages
	updateVie();

	//D�BUT DU COMBAT
	srand(time(NULL));
	Qt::Alignment alignement(Qt::AlignTop);
	joueurActuel = (rand() % 2);
	titre = new QLabel("Au tour de " + QString::fromStdString(personnages[joueurActuel]->getNom()) + " de jouer (Joueur " + QString::number(joueurActuel + 1) + ")!");
	layoutTitre = new QVBoxLayout();
	layoutTitre->addSpacing(325);
	layoutTitre->addWidget(titre, 0, Qt::AlignCenter);
	titre->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	attaquesDroite = new QLabel("Hello", this);
	attaquesGauche = new QLabel("Hello", this);
	layoutAttaques = new QHBoxLayout();
	layoutTitre->addLayout(layoutAttaques);
	layoutAttaques->addWidget(attaquesGauche, 0, Qt::AlignLeft);
	layoutAttaques->addWidget(attaquesDroite, 0, Qt::AlignRight);
	layoutTitre->setAlignment(Qt::AlignTop);
	this->setLayout(layoutTitre);
	titre->setFont(fTitre);
	titre->setPalette(paletteTitre);
	texteAction->setFont(fAction);
	texteAction->setPalette(paletteAction);
	texteAction->move(725, 850);
	texteAction->adjustSize();
	texteAction->setWordWrap(true);
	texteAction->setFixedSize(600, 200);
	attaquesGauche->setText("Attaques de " + QString::fromStdString(personnages[0]->getNom()));
	attaquesDroite->setText("Attaques de " + QString::fromStdString(personnages[1]->getNom()));

	attaquesDroite->setFont(fAction);
	attaquesGauche->setFont(fAction);
	
	if (personnages[0]->getAttaque(0) != nullptr)
	{
		attaquesGauche->setText(attaquesGauche->text() + "\n1: " + QString::fromStdString(personnages[0]->getAttaque(0)->getNom())
			+ " (Type " + QString::fromStdString(personnages[0]->getAttaque(0)->getNomType()) + ")");
	}
	if (personnages[0]->getAttaque(1) != nullptr)
	{
		attaquesGauche->setText(attaquesGauche->text() + "\n2: " + QString::fromStdString(personnages[0]->getAttaque(1)->getNom())
			+ " (Type " + QString::fromStdString(personnages[0]->getAttaque(1)->getNomType()) + ")");
	}
	if (personnages[0]->getAttaque(2) != nullptr)
	{
		attaquesGauche->setText(attaquesGauche->text() + "\n3: " + QString::fromStdString(personnages[0]->getAttaque(2)->getNom())
			+ " (Type " + QString::fromStdString(personnages[0]->getAttaque(2)->getNomType()) + ")");
	}
	if (personnages[0]->getAttaque(3) != nullptr)
	{
		attaquesGauche->setText(attaquesGauche->text() + "\n4: " + QString::fromStdString(personnages[0]->getAttaque(3)->getNom())
			+ " (Type " + QString::fromStdString(personnages[0]->getAttaque(3)->getNomType()) + ")");
	}

	if (personnages[1]->getAttaque(0) != nullptr)
	{
		attaquesDroite->setText(attaquesDroite->text() + "\n1: " + QString::fromStdString(personnages[1]->getAttaque(0)->getNom())
			+ " (Type " + QString::fromStdString(personnages[1]->getAttaque(0)->getNomType()) + ")");
	}
	if (personnages[1]->getAttaque(1) != nullptr)
	{
		attaquesDroite->setText(attaquesDroite->text() + "\n2: " + QString::fromStdString(personnages[1]->getAttaque(1)->getNom())
			+ " (Type " + QString::fromStdString(personnages[1]->getAttaque(1)->getNomType()) + ")");
	}
	if (personnages[1]->getAttaque(2) != nullptr)
	{
		attaquesDroite->setText(attaquesDroite->text() + "\n3: " + QString::fromStdString(personnages[1]->getAttaque(2)->getNom())
			+ " (Type " + QString::fromStdString(personnages[1]->getAttaque(2)->getNomType()) + ")");
	}
	if (personnages[1]->getAttaque(3) != nullptr)
	{
		attaquesDroite->setText(attaquesDroite->text() + "\n4: " + QString::fromStdString(personnages[1]->getAttaque(3)->getNom())
			+ " (Type " + QString::fromStdString(personnages[1]->getAttaque(3)->getNomType()) + ")");
	}
	cout << personnages[0]->getAttaque(3)->getDegats() << endl;

	/*layoutTitre->addLayout(layoutBonhommes);
	layoutBonhommes->addWidget(persoP1);
	layoutBonhommes->addWidget(persoP2);*/

}

Combat::~Combat()
{
	
}

QPalette Combat::getBackground()
{
	return paletteBackground;
}

void Combat::updateVie()
{
	HPPlayer1->setValue(personnages[0]->getVie());
	HPPlayer2->setValue(personnages[1]->getVie());

	if (personnages[0]->getPtsDefense() > 0)
	{
		DefPlayer1->setValue(personnages[0]->getPtsDefense());
	}

	if (personnages[1]->getPtsDefense() > 0)
	{
		DefPlayer2->setValue(personnages[1]->getPtsDefense());
	}
}

void Combat::keyPressEvent(QKeyEvent *event)
{
	srand(time(NULL));
	int random = rand() % personnages[joueurActuel]->getRepliques()->getTaille();
	texteAction->setText(QString::fromStdString(personnages[joueurActuel]->getNom()) + " dit: " + 
		QString::fromStdString(personnages[joueurActuel]->getRepliques()->operator[](random)->getReplique()));
	texteAction->adjustSize();
	if (personnages[0]->getVie() < 1 || personnages[1]->getVie() < 1)
	{
		boutonContinuer->setVisible(true);
		if (personnages[0]->getVie() > 0)
		{
			gagnant = personnages[0];
		}
		else if (personnages[1]->getVie() > 0)
		{
			gagnant = personnages[1];
		}
		return;
	}
	if (event->key() == Qt::Key_1)
	{
		if (personnages[joueurActuel]->getAttaque(0) != nullptr)
		{
			if (joueurActuel == 0)
			{
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[1]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(0)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(0)->getNomType()) + ")\n");
				personnages[0]->attaquer(0);
				joueurActuel = 1;
			} 
			else {
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[0]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(0)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(0)->getNomType()) + ")\n");
				personnages[1]->attaquer(0);
				joueurActuel = 0;
			}
		}
		else {
			texteAction->setText("Le personnage n'a pas cette action, vous perdez \n votre tour!");
			if (joueurActuel == 0)
			{
				joueurActuel = 1;
			}
			else {
				joueurActuel = 0;
			}
		}
	}
	else if (event->key() == Qt::Key_2)
	{
		if (personnages[joueurActuel]->getAttaque(1) != nullptr)
		{
			if (joueurActuel == 0)
			{
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[1]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(1)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(1)->getNomType()) + ")\n");
				personnages[0]->attaquer(1);
				joueurActuel = 1;
			}
			else {
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[0]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(1)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(1)->getNomType()) + ")\n");
				personnages[1]->attaquer(1);
				joueurActuel = 0;
			}
		}
		else {
			texteAction->setText("Le personnage n'a pas cette action, vous perdez \n votre tour!");
			if (joueurActuel == 0)
			{
				joueurActuel = 1;
			}
			else {
				joueurActuel = 0;
			}
		}
	}
	else if (event->key() == Qt::Key_3)
	{
		if (personnages[joueurActuel]->getAttaque(2) != nullptr)
		{
			
			if (joueurActuel == 0)
			{
				personnages[0]->attaquer(2);
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque " 
					+ QString::fromStdString(personnages[1]->getNom()) + " avec l'attaque\n" 
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(2)->getNom()) + " (Type " + 
					QString::fromStdString(personnages[joueurActuel]->getAttaque(2)->getNomType()) + ")\n");
				joueurActuel = 1;
			}
			else {
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[0]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(2)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(2)->getNomType()) + ")\n");
				personnages[1]->attaquer(2);
				joueurActuel = 0;
			}
		}
		else {
			texteAction->setText("Le personnage n'a pas cette action, vous perdez \n votre tour!");
			if (joueurActuel == 0)
			{
				joueurActuel = 1;
			}
			else {
				joueurActuel = 0;
			}
		}
	}
	else if (event->key() == Qt::Key_4)
	{
		if (personnages[joueurActuel]->getAttaque(3) != nullptr)
		{
			if (joueurActuel == 0)
			{
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[1]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(3)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(3)->getNomType()) + ")\n");
				personnages[0]->attaquer(3);
				joueurActuel = 1;
			}
			else {
				texteAction->setText(texteAction->text() + "\n" + QString::fromStdString(personnages[joueurActuel]->getNom()) + " attaque "
					+ QString::fromStdString(personnages[1]->getNom()) + " avec l'attaque\n"
					+ QString::fromStdString(personnages[joueurActuel]->getAttaque(3)->getNom()) + " (Type " +
					QString::fromStdString(personnages[joueurActuel]->getAttaque(3)->getNomType()) + ")\n");
				personnages[1]->attaquer(3);
				joueurActuel = 0;
			}
		}
		else {
			texteAction->setText("Le personnage n'a pas cette action, vous perdez \n votre tour!");
			if (joueurActuel == 0)
			{
				joueurActuel = 1;
			}
			else {
				joueurActuel = 0;
			}
		}
	}
	else {
		texteAction->setText("Le personnage n'a pas cette action, vous perdez \n votre tour!");
		if (joueurActuel == 0)
		{
			joueurActuel = 1;
		}
		else {
			joueurActuel = 0;
		}
	}
	titre->setText("Au tour de " + QString::fromStdString(personnages[joueurActuel]->getNom()) + " de jouer (Joueur " + QString::number(joueurActuel+1) + ")!");
	texteAction->adjustSize();
	updateVie();
	if (personnages[0]->getVie() < 1 || personnages[1]->getVie() < 1)
	{
		boutonContinuer->setVisible(true);
		if (personnages[0]->getVie() > 0)
		{
			gagnant = personnages[0];
		}
		else if (personnages[1]->getVie() > 0)
		{
			gagnant = personnages[1];
		}
		return;
	}
}

void Combat::setBoutonContinuer(QPushButton* bouton)
{
	boutonContinuer = bouton;
	boutonContinuer->setVisible(false);
}

int Combat::getWinner()
{
	if (gagnant->getNom() == jp->getNom())
	{
		return 1;
	}
	else if (gagnant->getNom() == claudette->getNom())
	{
		return 2;
	}
	else if (gagnant->getNom() == etudiant->getNom())
	{
		return 3;
	}
	else if (gagnant->getNom() == gro->getNom())
	{
		return 4;
	}
	return 0;
}
