#include "type.h"

Type::Type(string nom1, Type* faiblesse1, double faiblessePuissance1, double force1, Type::lesTypes enumType1)
{
    nom = nom1;
    faiblesse = faiblesse1;
    faiblessePuissance = faiblessePuissance1;
    force = force1;
    enumType = enumType1;
}

Type* Type::getFaiblesse()
{
    return faiblesse;
}

std::string Type::getNom()
{
    return nom;
}

double Type::getFaiblessePuissance()
{
    return faiblessePuissance;
}

double Type::getForce()
{
    return force;
}

Type::lesTypes Type::getEnumType()
{
    return enumType;
}