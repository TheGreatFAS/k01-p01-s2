OFILES = Attaque.o contreAttaque.o Personnage.o replique.o type.o menu.o ConsoleColor.h

make: $(OFILES)
	g++ -o VocalKombatExec VocalKombat.cpp $(OFILES)

Attaque.o: Attaque.cpp Attaque.h type.h vecteur.h
	g++ -c -o Attaque.o Attaque.cpp

contreAttaque.o: contreAttaque.h contreAttaque.cpp
	g++ -c -o contreAttaque.o contreAttaque.cpp

Personnage.o: Personnage.h Personnage.cpp vecteur.h type.h Attaque.h contreAttaque.h replique.h
	g++ -c -o Personnage.o Personnage.cpp

replique.o: replique.h replique.cpp ConsoleColor.h
	g++ -c -o replique.o replique.cpp

type.o: type.h type.cpp
	g++ -c -o type.o type.cpp

menu.o: menu.h menu.cpp vecteur.h Personnage.h type.h ConsoleColor.h
	g++ -c -o menu.o menu.cpp


clean:
	rm -f *.o 
	rm -f VocalKombatExec
