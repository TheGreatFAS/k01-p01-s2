#ifndef MAINUI_H
#define MAINUI_H

#include <iostream>
#include <QMainWindow>
#include <QtWidgets>
#include "MenuSelectionJoueur.h"
#include "Combat.h"
#include "maintitle.h"
#include "win.h"


using namespace std;

class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		MainWindow(QWidget *parent = nullptr);
		~MainWindow();
		QPushButton* getMainButton();
		QWidget* getMainWidget();
	private slots:
		void afficherMenuSelection();
		void afficherCombat();
		void afficherMenuPrincipal();
		void afficherMenuPrincipalSelection();
		void afficherMenuPrincipalFinal();
		void afficherFinal();
	private:
		QPushButton *bouton;
		MenuSelectionJoueur *Menu;
		Maintitle* menuPrincipal;
		QWidget* mainWidget;
		QPushButton* boutonCancelSelection;
		QPalette paletteBackground;
		Combat* combat;
		win *Win;
		QPushButton* boutonCancelFinal;
		QPushButton* continuerCombat;
		QFont fontBouton;
};
#endif