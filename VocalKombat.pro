QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += Main.cpp \
	MainUI.cpp \
	maintitle.cpp \
 	MenuSelectionJoueur.cpp \
 	Personnages/Personnage.cpp \
	Personnages/Claudette.cpp \
	Personnages/JP.cpp \
	Personnages/Etudiant.cpp \
	Personnages/GRO.cpp \
 	Attaque.cpp \
 	contreAttaque.cpp \
 	menu.cpp \
	replique.cpp \ 
	type.cpp \
    	Combat.cpp \
	win.cpp\
	maintitle.cpp\
	

HEADERS += MainUI.h \
	maintitle.h \
	MenuSelectionJoueur.h \
	Personnages/Personnage.h \
	Personnages/Claudette.h \
	Personnages/JP.h \
	Personnages/Etudiant.h \
	Personnages/GRO.h \
	vecteur.h \
	Attaque.h \
	contreAttaque.h \
	ConsoleColor.h \
	menu.h \
	replique.h \
	type.h \
    	Combat.h \
	win.h \
	maintitle.h
	

TEMPLATE = vcapp
TARGET = VocalKombat

CONFIG += warn_on qt debug_and_release windows console

INCLUDEPATH += .



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
