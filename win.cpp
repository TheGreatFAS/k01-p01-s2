#include"win.h"
win::win(int winnerr, QWidget* parent) : QWidget(parent)
{
	//initialisation
	mainwidget = new QWidget(this);
	boutwidget = new QWidget(mainwidget);
	layoutprincipal = new QVBoxLayout(mainwidget);
	QPalette palette;
	QPixmap background("./Images/Final.png");
	background.scaled(this->size(), Qt::IgnoreAspectRatio);
	paletteBackGround.setBrush(QPalette::Background, background);
	this->setPalette(paletteBackGround);
	espace = new QWidget(boutwidget);
	espace2 = new QWidget(boutwidget);
	winner = new QLabel(boutwidget);
	espace3 = new QWidget(boutwidget);
	
	switch (winnerr)
	{
	case 1:
	{
		QPixmap winimg("./Images/PhotosPersonnages/Selection/JP.png");
		winner->setPixmap(winimg);
		break;
	}
	case 2:
	{
		QPixmap winimg("./Images/PhotosPersonnages/Selection/Claudette.png");
		winner->setPixmap(winimg);
		break;
	}
	case 3:
	{
		QPixmap winimg("./Images/PhotosPersonnages/Selection/Etudiant.png");
		winner->setPixmap(winimg);
		break;
	}
	case 4:
	{
		QPixmap winimg("./Images/PhotosPersonnages/Selection/GRO.png");
		winner->setPixmap(winimg);
		break;
	}
	}


	



	layoutprincipal->addWidget(winner,0,Qt::AlignCenter);
	
	mainwidget->setLayout(layoutprincipal);
	



	
}

QPalette win::getBackground()
{
	return paletteBackGround;
}

QWidget* win::getMainWidget()
{
	return mainwidget;
}

win::~win()
{

}


//QPixmap win::setwinner()
//{
//	switch (winnerr)
//	{
//	case 1:
//	{
//		QPixmap winimg("./Images/JP.png");
//		break;
//	}
//	case 2:
//	{
//		QPixmap winimg("./Images/Claudette.png");
//		break;
//	}
//	case 3:
//	{
//		QPixmap winimg("./Images/Etudiant.png");
//		break;
//	}
//	case 4:
//	{
//		QPixmap winimg("./Images/GRO.png");
//		break;
//	}
//}
