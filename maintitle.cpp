#include "maintitle.h"

Maintitle::Maintitle(QWidget* parent) : QWidget(parent)
{

	QLabel* image = new QLabel;
	//QPixmap bkgnd("./Images/maintitle.png");
	//image->setPixmap(bkgnd.scaled(1920, 1080, Qt::IgnoreAspectRatio));
	//image->setScaledContents(true);

	QPixmap backGroundImage("./Images/maintitle.png");
	backGroundImage.scaled(this->size(), Qt::IgnoreAspectRatio);
	paletteBackGround.setBrush(QPalette::Background, backGroundImage);

	this->setWindowTitle("Vocal Kombat");
	button1 = new QPushButton("Commencer une partie!");
	button2 = new QPushButton("Quitter");
	button1->setFixedHeight(50);
	button1->setFixedWidth(700);
	button2->setFixedHeight(50);
	button2->setFixedWidth(700);
	
	quit = new QAction("&Quitter", this);
	menubar = new QMenuBar(this);

	File = menubar->addMenu("&File");
	File->addAction(quit);
	menubar->adjustSize();

	connect(button2, SIGNAL(clicked()), this, SLOT(exit()));
	connect(quit, SIGNAL(triggered()), this, SLOT(exit()));

	layout = new QGridLayout(this);
	layout->addWidget(image, 1, 0);
	layout->addWidget(button1, 2, 0, 1, 1);
	layout->addWidget(button2, 3, 0, 1, 1);

	

	this->setLayout(layout);
}

Maintitle::~Maintitle()
{
	delete button1;
	delete button2;
	delete quit;
	delete menubar;
	delete layout;
}

void Maintitle::exit()
{
	QApplication::quit();
}

QPushButton* Maintitle::getBoutonDemarrer()
{
	return button1;
}

QPalette Maintitle::getBackground()
{
	return paletteBackGround;
}
