#pragma once

#include "Personnages/Personnage.h"
#include "Personnages/Claudette.h"
#include "Personnages/JP.h"
#include "Personnages/GRO.h"
#include "Personnages/Etudiant.h"
#include "vecteur.h"
#include <QDebug>
#include <iostream>
#include <QMainWindow>
#include <QtWidgets>
#include <QKeyEvent>
#include "KeyPress.h"
#include "Attaque.h"
#include <stdlib.h>
#include <time.h>

using namespace std;

class Combat
	: public QWidget
{
	Q_OBJECT
	public:
		Combat(QWidget* parent, Vecteur<Personnage*>);
		~Combat();
		QPalette getBackground();
		void setBoutonContinuer(QPushButton*);
		int getWinner();

	private:
		Vecteur<Personnage*> personnages;
		QProgressBar* HPPlayer1;
		QProgressBar* HPPlayer2;
		QProgressBar* DefPlayer1;
		QProgressBar* DefPlayer2;
		QPalette paletteBackground;
		void updateVie();
		int joueurActuel;
		int choixAttaque = 1;
		void keyPressEvent(QKeyEvent *);
		QLabel* titre;
		QVBoxLayout* layoutTitre;
		QFont fTitre;
		QLabel* texteAction;
		QFont fAction;
		QLabel* attaquesGauche;
		QLabel* attaquesDroite;
		QHBoxLayout* layoutAttaques;
		QPushButton* boutonContinuer;
		Personnage* gagnant;
		JP* jp;
		Claudette* claudette;
		Etudiant* etudiant;
		GRO* gro;
		QWidget* persoP1;
		QWidget* persoP2;
		QHBoxLayout* layoutBonhommes;
		
	/*private slots:*/

};