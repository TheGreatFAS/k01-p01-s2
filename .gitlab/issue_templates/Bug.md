### Sommaire

(Insérez le sommaire du bug de façon concise)

### Étapes à reproduire

(Comment peut-on reproduire le bug - très important)

### Quel est le comportement actuel du *bug*?

(Qu'est-ce qui arrive)

### Quel est le comportement *correct* attendu?

(Comportement attendu)

### Correction possible

(Si possible, liez la ligne de code responsable du *bug*)

/label ~Bug
