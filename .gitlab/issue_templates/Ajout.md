### **Sommaire**
(Sommaire de l'ajout)

### **Type** d'ajout
- [ ] Ajout de **code**
- [ ] Ajout à **l'analyse**
- [ ] Ajout de **diagramme**
- [ ] Ajout de **commentaire**
- [ ] Ajout de **test**
- [ ] Ajout de **documentation**

### **Nécessité** de l'ajout
(Expliquez en quoi cet ajout est **nécessaire**)

### Résultat espéré (*facultatif*)
(Expliquez le résultat espéré de cet ajout)

## Chemin du fichier
(Expliquez où se trouverait l'ajout ou liez le fichier associé *(facultatif)*)

/label ~Ajout