### Sommaire

(Insérez le sommaire de l'erreur de documentation)

### Type
- [ ] Manque de documentation
- [ ] Erreur dans la documentation
- [ ] Faute de français dans la documentation
- [ ] Mauvais standard de documentation

### Modification à apporter

(Insérez si possible une suggestion de la modification à apporter)

### Ligne de l'erreur de documentation (facultatif)

(Insérez la ligne de l'erreur de documentation)

### Chemin du fichier (facultatif)

(Insérez le chemin du fichier contenant l'erreur de documentation)


/label ~Documentation
