### **Sommaire**
(Sommaire de la suppression)

### **Type** de suppression
- [ ] Suppression de **code**
- [ ] Suppression à **l'analyse**
- [ ] Suppression de **diagramme**
- [ ] Suppression de **commentaire**
- [ ] Suppression de **test**
- [ ] Suppression de **documentation**

### **Nécessité** de la suppression
(Expliquez en quoi cette suppression est **nécessaire**)

### Résultat espéré (*facultatif*)
(Expliquez le résultat espéré de cette suppression)

## Chemin du fichier
(Expliquez où se trouverait la suppression ou liez le fichier associé *(facultatif)*)

/label ~Suppression