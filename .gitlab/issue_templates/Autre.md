### Sommaire
    
    (Insérez le sommaire de l'issue de façon concise et claire)
    
### Partie du projet touchée

- [ ] Analyse
- [ ] Code (BackEnd)
- [ ] Interface (FrontEnd)
- [ ] Documentation
- [ ] Commentaires du code
- [ ] Standard de programmation
    
### Nécessité
    
    (Indiquez la nécessité de cet issue)
    
### Provenance (facultatif)
    
    (Indiquez les causes de la création de cet issue)
    
### Amélioration
    
    (Indiquez en quoi cet issue va améliorer le projet)
    
### Résultat attendu (facultatif)
    (Si applicable, expliquez le résultat attendu)

### Chemin de fichier (facultatif)

    (Insérez si possible le chemin du fichier, voir la ligne où l'issue s'applique)
    
/label ~Autre

    
