### Sommaire

    (Insérez le sommaire de la vulnérabilité de façon concise)

### Étapes à reproduire

    (Comment peut-on reproduire la vulnérabilité - très important)

### Quel est le comportement actuel de la vulnérabilité?

    (Qu'est-ce qui arrive)

### Quel est le comportement *correct* attendu?

    (Comportement attendu)
    
### Chemin du fichier

    (Indiquez le fichier à modifier (le lier si possible))

### Correction possible

    (Si possible, liez la ligne de code responsable de cette vulnérabilité)

/label ~Vulnerabilite