#pragma once
#include "Personnage.h"

class Claudette : public Personnage
{
public:
	Claudette();
	Attaque* attaqueClaudette1;
	Attaque* attaqueClaudette2;
	Attaque* attaqueClaudette3;
	Attaque* attaqueClaudette4;
	Replique* repliqueClaudette1;
	Replique* repliqueClaudette2;
	Replique* repliqueClaudette3;
	Replique* repliqueClaudette4;
};