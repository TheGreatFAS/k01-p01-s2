#include "GRO.h"

GRO::GRO()
{
	nom = "GRO";
	type = typeGRO;
	ptsVie = 1;
	ptsDefense = 99999999;
	attaqueGRO1 = new Attaque(typeDef, typeProf, 2, "M�diocrit� dans tous les domaines");
	repliqueGRO1 = new Replique("Ma c�te R est trop bonne!");
	repliqueGRO2 = new Replique("Chu full polyvalent.");
	ajouterAttaque(attaqueGRO1);
	ajouterReplique(repliqueGRO1);
	ajouterReplique(repliqueGRO2);
	pathToSpriteTitle = "./Images/PhotosPersonnages/Selection/GRO.png";
	pathToSpriteP1 = "./Images/PhotosPersonnages/Players/GRO P1.png";
	pathToSpriteP2 = "./Images/PhotosPersonnages/Players/GRO P2.png";
}