#include "Personnage.h"

//Personnage::Personnage(string nom1, Type* type1, int ptsVie1, int defense)
//{
//    nom = nom1;
//    type = type1;
//    ptsVie = ptsVie1;
//    ptsDefense = defense;
//}

Personnage::~Personnage()
{
    delete repliques;
    delete attaques;
}

void Personnage::setAdversaire(Personnage* personne)
{
    adversaire = personne;
}

void Personnage::attaquer(int index)
{
    if(index < attaques->getTaille())
    {
        cout << endl << endl << endl;
        int degatsTemp = 0;
        //Set une variable aléatoire entre 0 et
        //la taille du vecteur et affiche la replique à l'index
        srand(time(NULL));
        int random = rand()%repliques->getTaille();

        //Affichage de la réplique
        cout << nom << " dit: " << (repliques->operator[](random))->getReplique() << endl;

        Attaque* attaqueTemp = attaques->operator[](index);
        //Vérifie si l'attaque est défensive (Shield) ou offensive
        if(attaqueTemp->getEnumType() == Type::lesTypes::Defense)
        {
            if(ptsDefense < 85)
            {
                ptsDefense += 15;
            }else
            {
                ptsDefense = 100;
            }
            cout << CONSOLECOLOR_BLUE << "L'attaque est défensive, " << nom << " a maintenant " << ptsDefense << " points de défense!" << CONSOLECOLOR_WHITE << endl;
        }

        if(adversaire->getFaiblesse() != nullptr)
        {
            if(type == adversaire->getFaiblesse())
            {
                degatsTemp = attaqueTemp->getDegats()
                * adversaire->getFaiblessePuissance();
            } else
            {
                degatsTemp = attaqueTemp->getDegats();
            }
        } else
        {
            degatsTemp = attaqueTemp->getDegats();
        }
        

        if(attaqueTemp->getType() == adversaire->getFaiblesse())
        {
            //IMPLÉMENTER LES DIFFÉRENTES 
            //VALEURS DE FAIBLESSE ET 
            //MULTIPLIÉ LES DÉGATS
        }
        //IMPLÉMENTER LES DIFFERENTES SORTIES DE CONSOLE
        //(EX.:SI DEGATS < 5, cout << 
        //C'EST TRÈS INEFFICACE!, etc)
        
        cout << CONSOLECOLOR_YELLOW << endl << nom << "(Type " << type->getNom() << ")"
        << " attaque " << adversaire->getNom() 
        << "(Type " << adversaire->getType()->getNom() << ") "
        << "avec l'attaque " << 
        attaqueTemp->getNom() << "(Type " << 
        attaqueTemp->getNomType() << ")" << CONSOLECOLOR_YELLOW << endl;
        
        //Gestion aléatoire des dégats (+ ou - 3 points)
        srand(time(NULL));
        int aleatoire = rand()%7;
        cout << CONSOLECOLOR_CYAN << "Nombre randomiser de points de dégat: " << aleatoire << CONSOLECOLOR_WHITE << endl;
        switch (aleatoire)
        {
        case 0:
            //Dégats de base - 3 points
            degatsTemp -= 3;
            break;
        case 1:
            //Dégats de base - 2 points
            degatsTemp -= 2;
            break;

        case 2:
            //Dégats de base - 1 points
            degatsTemp -= 1;
            break;

        case 3:
            //Dégats de base + 3 points
            degatsTemp += 3;
            break;

        case 4:
            //Dégats de base + 2 points
            degatsTemp += 2;
            break;

        case 5:
            //Dégats de base + 1 points
            degatsTemp += 1;
            break;

        case 6:;
            //Dégats de base
            break;
        }

        if(adversaire->getPtsDefense() > 0)
        {
            cout << CONSOLECOLOR_BLUE;
            if(adversaire->getPtsDefense() < degatsTemp)
            {
                cout << adversaire->getNom() << " perd " << adversaire->getPtsDefense()
                << " points de défense." << endl;
                degatsTemp -= adversaire->getPtsDefense();
                adversaire->setPtsDefense(0);
            } else
            {
                adversaire->setPtsDefense(adversaire->getPtsDefense() - degatsTemp);
                cout << adversaire->getNom() << " perd " << degatsTemp
                << " points de défense." << endl;
                degatsTemp = 0;
            }
            cout << endl << adversaire->getNom() << " a maintenant " << adversaire->getPtsDefense() << " points de défense!" << CONSOLECOLOR_WHITE << endl;
        }
        adversaire->setVie(adversaire->getVie() - degatsTemp);
    }
    else
    {
        cout << 
        "Il n'y a aucune attaque à cette position!"
        " Veuillez entrer un index valide." 
        << endl;
    }
    
}

double Personnage::getFaiblessePuissance()
{
    return type->getFaiblessePuissance();
}

Type* Personnage::getFaiblesse()
{
    return type->getFaiblesse();
}

Vecteur<Attaque*>* Personnage::getAttaques()
{
    return attaques;
}

Attaque* Personnage::getAttaque(int index)
{
    return attaques->operator[](index);
}

string Personnage::getNom()
{
    return nom;
}

int Personnage::getVie()
{
    return ptsVie;
}

void Personnage::setVie(int nouvVie)
{
    if(nouvVie > ptsVie)
    {
        cout << CONSOLECOLOR_GREEN << "La vie de " << nom << " passe de "
        << ptsVie << " points à " << nouvVie << " points ("
        << nouvVie - ptsVie << " points de vie gagnés)!"
        << CONSOLECOLOR_WHITE << endl << endl << endl;
    }
    else
    {
        cout << CONSOLECOLOR_RED << "La vie de " << nom << " passe de " 
        << ptsVie << " points à " << nouvVie << " points ("
        << ptsVie - nouvVie << " points de vie perdus)!"
        << CONSOLECOLOR_WHITE << endl << endl << endl;
    }
    ptsVie = nouvVie;
    //Implementer les animations de perte de vie
}

int Personnage::getPtsDefense()
{
    return ptsDefense;
}

void Personnage::setPtsDefense(int nouvDefense)
{
    ptsDefense = nouvDefense;
}

void Personnage::ajouterAttaque(Attaque* nouvAttaque)
{
    attaques->operator+=(nouvAttaque);
}

void Personnage::ajouterReplique(Replique* nouvReplique)
{
    repliques->operator+=(nouvReplique);
}

Type* Personnage::getType()
{
    return type;
}

Type::lesTypes Personnage::getEnumType()
{
    return type->getEnumType();
}

Vecteur<Replique*>* Personnage::getRepliques()
{
	return repliques;
}


