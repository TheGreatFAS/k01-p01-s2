#include "Etudiant.h"

Etudiant::Etudiant()
{
	nom = "Étudiant";
	type = typeDepression;
	ptsVie = 50;
	ptsDefense = 100;
	attaqueEtudiant1 = new Attaque(typePhysique, nullptr, 7, "Uppercut");
	attaqueEtudiant2 = new Attaque(typeDepression, typePsy, 10, "Pleures");
	attaqueEtudiant3 = new Attaque(typeDef, nullptr, 0, "Faire les lectures");
	repliqueEtudiant1 = new Replique("I can't live with myself...");
	repliqueEtudiant2 = new Replique("'G'RO hahaha");
	repliqueEtudiant3 = new Replique("C'est trivial!");
	ajouterAttaque(attaqueEtudiant1);
	ajouterAttaque(attaqueEtudiant2);
	ajouterAttaque(attaqueEtudiant3);
	ajouterReplique(repliqueEtudiant1);
	ajouterReplique(repliqueEtudiant2);
	ajouterReplique(repliqueEtudiant3);
	pathToSpriteTitle = "./Images/PhotosPersonnages/Selection/Etudiant.png";
	pathToSpriteP1 = "./Images/PhotosPersonnages/Players/Etudiant P1.png";
	pathToSpriteP2 = "./Images/PhotosPersonnages/Players/Etudiant P2.png";
}
