#include "JP.h"

JP::JP()
{
	nom = "Jean-Phillipe (J-P)";
	type = typeProf; 
	ptsVie = 100;
	ptsDefense = 0;
	attaqueJP1 = new Attaque(typePsy, typePhysique, 12, "Coup d'pied dans gorge!");
	repliqueJP1 = new Replique("Mon nom est Jean-Philippe!");
	ajouterAttaque(attaqueJP1);
	ajouterReplique(repliqueJP1);
	pathToSpriteTitle = "./Images/PhotosPersonnages/Selection/JP.png";
	pathToSpriteP1 = "./Images/PhotosPersonnages/Players/JP P1.png";
	pathToSpriteP2 = "./Images/PhotosPersonnages/Players/JP P2.png";
}