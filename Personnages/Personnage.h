#ifndef PERSONNAGE_H
#define PERSONNAGE_H

#include <iostream>
#include <time.h>
#include "../vecteur.h"
#include "../type.h"
#include "../Attaque.h"
#include "../contreAttaque.h"
#include "../replique.h"
#include "../ConsoleColor.h"

using namespace std;

class Personnage
{
    public:
        virtual void ajouterAttaque(Attaque*);
        virtual void ajouterReplique(Replique*);
		virtual void attaquer(int);
        virtual Vecteur<Attaque*>* getAttaques();
		virtual Attaque* getAttaque(int);
		virtual string getNom();
		virtual int getVie();
		virtual void setVie(int);
		virtual void setAdversaire(Personnage*);
		virtual double getFaiblessePuissance();
		virtual Type* getFaiblesse();
		virtual Type* getType();
		virtual int getPtsDefense();
		virtual void setPtsDefense(int);
		virtual Type::lesTypes getEnumType();
		virtual Vecteur<Replique*>* getRepliques();
		string pathToSpriteTitle;
		string pathToSpriteP1;
		string pathToSpriteP2;
		~Personnage();

    protected:
		Type* typePhysique = new Type("Physique", nullptr, 0, 1.1, Type::lesTypes::Physique);
		Type* typePsy = new Type("Psychique", typePhysique, 1.2, 1.5, Type::lesTypes::Psychique);
		Type* typeDepression = new Type("Dépression", typePsy, 1.6, 1.34, Type::lesTypes::Depression);
		Type* typeProf = new Type("Professeur", typePhysique, 1.34, 1.7, Type::lesTypes::Physique);
		Type* typeDef = new Type("Défense", typePsy, 0, 0, Type::lesTypes::Defense);
		Type* typeGRO = new Type("GRO", typeDepression, 50000000, 1, Type::lesTypes::Defense);
		Vecteur<Attaque*>* attaques = new Vecteur<Attaque*>;
		string nom;
		Type* type;
		Vecteur<ContreAttaque*>* contreAttaques = new Vecteur<ContreAttaque*>;
		Vecteur<Replique*>* repliques = new Vecteur<Replique*>;
        int ptsVie = 0;
        int ptsDefense = 0;
        Personnage* adversaire;
};

#endif