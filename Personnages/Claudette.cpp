#include "Claudette.h"

Claudette::Claudette()
{
    nom = "Claudette";
	type = typePhysique;
    ptsVie = 100;
    ptsDefense = 0;
	attaqueClaudette1 = new Attaque(typePhysique, typeDepression, 20, "Drop Kick");
	attaqueClaudette2 = new Attaque(typePsy, typePhysique, 15, "LE VOYEZ-VOUS?!?!");
	attaqueClaudette3 = new Attaque(typeProf, nullptr, 12, "Incompréhension");
	attaqueClaudette4 = new Attaque(typePhysique, typeDepression, 13, "Claque-porte");
	repliqueClaudette1 = new Replique("LE VOYEZ-VOUS?!?!?!?!?!");
	repliqueClaudette2 = new Replique("Claudette va continuer à être 'Môman'!");
	repliqueClaudette3 = new Replique("Ha cr**s, je l'ai pas lancé à la bonne place...");
	repliqueClaudette4 = new Replique("Vas-y tranquillement, mais DÉPÊCHE-TOÉ!");
	ajouterAttaque(attaqueClaudette1);
	ajouterAttaque(attaqueClaudette2);
	ajouterAttaque(attaqueClaudette3);
	ajouterAttaque(attaqueClaudette4);
	ajouterReplique(repliqueClaudette1);
	ajouterReplique(repliqueClaudette2);
	ajouterReplique(repliqueClaudette3);
	ajouterReplique(repliqueClaudette4);
	pathToSpriteTitle = "./Images/PhotosPersonnages/Selection/Claudette.png";
	pathToSpriteP1 = "./Images/PhotosPersonnages/Players/Claudette P1.png";
	pathToSpriteP2 = "./Images/PhotosPersonnages/Players/Claudette P2.png";
}

