#pragma once
#include "Personnage.h"

class Etudiant :public Personnage
{
public:
	Etudiant();
	Attaque* attaqueEtudiant1;
	Attaque* attaqueEtudiant2;
	Attaque* attaqueEtudiant3;
	Replique* repliqueEtudiant1;
	Replique* repliqueEtudiant2;
	Replique* repliqueEtudiant3;
};