#pragma once
#include "Personnage.h"

class GRO : public Personnage
{
public:
	GRO();
	Attaque* attaqueGRO1;
	Replique* repliqueGRO1;
	Replique* repliqueGRO2;
};