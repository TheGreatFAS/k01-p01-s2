#include "Attaque.h"

Attaque::Attaque(Type* type1, Type* typeFaiblesse, int degats1, string nom1)
{
    type = type1;
    faiblesse = typeFaiblesse;
    degats = degats1;
    nom = nom1;
}

Type* Attaque::getFaiblesse()
{
    return faiblesse;
}

int Attaque::getDegats()
{
    return degats;
}

Type* Attaque::getType()
{
    return type;
}

string Attaque::getNom()
{
    return nom;
}

std::string Attaque::getNomType()
{
    return type->getNom();
}

Type::lesTypes Attaque::getEnumType()
{
    return type->getEnumType();
}