#pragma once
using namespace std;
#include <iostream>
#include "type.h"
#include "vecteur.h"
#include "Personnages/Personnage.h"
#include "Personnages/Claudette.h"
#include "Personnages/JP.h"
#include "Personnages/Etudiant.h"
#include "ConsoleColor.h"
class Menu 
{
public:
Menu();
void afficherAttaques(Personnage* perso1);
Personnage* characterSelect();

private:
Vecteur<Personnage*> personnages;
int player = 1;
};